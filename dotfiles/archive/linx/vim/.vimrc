" Vundle Stuff
set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Some of my Choice Plugins here
" Fugitive, Some fucking aweoms git integration:
Plugin 'tpope/vim-fugitive'

" Nerdtree, A nice wee file explorer.
Plugin 'scrooloose/nerdtree'

" Syntastic, Syntax checking awesomeness
Plugin 'scrooloose/syntastic'

" Fuzzy search for files, buffer mru etc.
Plugin 'kien/ctrlp.vim'

" Nice status tab line.
Plugin 'bling/vim-airline'

" Some code completion stuff.
Plugin 'valloric/youcompleteme'

call vundle#end()

" From here All the basics .. this shall go anywhere with me.
set modelines=0
set nocompatible
set number
set ruler
set visualbell
set encoding=utf-8
set wrap
set tabstop=4
set softtabstop=4
set expandtab
set laststatus=2
set showmode
set showcmd
set cursorline
set hlsearch
set smartcase
set showmatch
set ignorecase
set incsearch
set wildmenu
set lazyredraw
set backup
set backupdir=/tmp
set backupskip=/tmp/*
set directory=/tmp
set writebackup
set background=dark
"colorscheme solarized
filetype off
syntax on
