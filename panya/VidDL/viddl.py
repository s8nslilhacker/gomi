from urllib.request import urlretrieve
from multiprocessing import Pool, Manager
import os
import sys
import re
import subprocess

# Setup our params
url = sys.argv[1]
file_name = sys.argv[2]

# Setup our multiprocessing
manager = Manager()
mp_retry_list = manager.list()

# retry and fail lists:
second_retry_list = []
third_retry_list = []
failed_list = []

# Getting our chunks programmatically
def get_chunks():
    print(f"Getting index file to calculate chunks")
    try:
        urlretrieve(
            f"{url}index.m3u8",
            "index.m3u8"
        )
    except:
        print("Error downloading index file. Exiting")
        sys.exit(1)

    index_file = open("./index.m3u8")

    index_list = []
    for line in index_file:
        index_list.append(line)

    targ_line = index_list[-2]

    temp = re.findall(r'\d+', targ_line)

    no_of_chunks = int(temp[0])

    print(f"We have {no_of_chunks} chunks to download.")

    return no_of_chunks



# our downloader
def download_chunk(chunk_no: int):
    print("Checking if chunk exists")
    if not os.path.exists(f'./seg-{chunk_no}.ts'):
        print(f"Downloading chunk number: {chunk_no}")
        try:
            urlretrieve(
                f"{url}seg-{chunk_no}-v1-a1.ts",
                f"seg-{chunk_no}.ts"
            )
            print(f"Chunk no: {chunk_no} downloaded")
        except:
            print(f"Error Downloading Chunk {chunk_no}: Putting in retry list")
            mp_retry_list.append(chunk_no)
    else:
        print(f"Chunk no: {chunk_no} already exists. Skipping.")

def retry_chunk_dl(chunk_no: int, next_chunk_list: list):
    print(f"Processing the retry list {next_chunk_list}")
    print(f"Downloading chunk number: {chunk_no}")
    try:
        urlretrieve(
            f"{url}seg-{chunk_no}-v1-a1.ts",
            f"seg-{chunk_no}.ts"
        )
        print(f"Chunk no: {chunk_no} downloaded")
    except:
        print(f"Error Downloading Chunk {chunk_no}: Putting in next retry list")
        next_chunk_list.append(chunk_no)



def gen_ffmpeg_input_file():
    # print("Run the following to create an input file:")
    # print(f"for i in `seq 1 {chunks_arg}`; do echo file seg-$i.ts; done >> input_file.txt")
    print("Building input file for ffmpeg")
    build_file = open("input_file.txt", 'w+')
    for chunk in chunks_list:
        build_file.write(f"file seg-{chunk}.ts\n")

    build_file.close()

def concat_vid_files():
    print("Running the following to stitch the files together:")
    print(f"ffmpeg -f concat -safe 0 -i input_file.txt -c copy ../{file_name}.ts")
    concat = subprocess.run([
        "ffmpeg",
        "-f",
        "concat",
        "-safe",
        "0",
        "-i",
        "input_file.txt",
        "-c",
        "copy",
        f"../{file_name}.ts"
    ],
        capture_output=True
    )

    if concat.returncode == 0:
        print(concat.stdout.decode())
    else:
        print(f"Error: Video concat failed with error code: {concat.returncode}")
        print(f"Error_Msg: {concat.stderr.decode()}")
        sys.exit(3)


def encode_video():
    print("Running the following to encode the video into something more useful")
    print(f"ffmpeg -i ../name_of_file.ts -acodec copy -vcodec copy ../{file_name}.mp4")
    encode = subprocess.run([
        "ffmpeg",
        "-i",
        f"../{file_name}.ts",
        "-acodec",
        "copy",
        "-vcodec",
        "copy",
        f"../{file_name}.mp4"
    ])

    if encode.returncode == 0:
        print("Encoding Done")
    else:
        print(f"Error: Video encoding failed with error code: {encode.returncode}")
        print(f"Error_Msg: {encode.stderr.decode()}")

# Setting up our chunks list
no_of_chunks = get_chunks()
chunks_list = [chunk for chunk in range(1, no_of_chunks + 1)]

mp_pool = Pool(processes=16)

mp_pool.map(download_chunk, (chunks_list))

mp_pool.close()
mp_pool.join()

print("Finished the main downloading.")
print("Sorting through the retry list")

retry_list = list(mp_retry_list)

# Process failed downloads one at a time until the process finishes
while len(retry_list) != 0:
    retry_chunk = retry_list.pop(0)
    retry_chunk_dl(retry_chunk, second_retry_list)

print("Sorting through the second retry list")

while len(second_retry_list) != 0:
    retry_chunk = second_retry_list.pop(0)
    retry_chunk_dl(retry_chunk, third_retry_list)

print("Sorting through the third retry list")

while len(third_retry_list) != 0:
    retry_chunk = third_retry_list.pop(0)
    retry_chunk_dl(retry_chunk, failed_list)

if len(failed_list) != 0:
    print("Couldn't verify all files. The following chunks have failed to download")
    print(failed_list)
    sys.exit(2)
else:
    gen_ffmpeg_input_file()
    concat_vid_files()
    encode_video()


sys.exit(0)
