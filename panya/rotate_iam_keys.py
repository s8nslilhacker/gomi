#!/usr/bin/env python3

from pathlib import Path
import boto
# Some Global var's

home = str(Path.home())
creds_file = home + "/.aws/credentials"

class Creds:

    """ Creds object is used to process your existing credentials file. """

    def __init__(self, access_key_id, secret_access_key):
        """
        Usual constructor, Here we initialise the Current Access Key ID, Secret key.
        We also create some place holders for the New ones we'll fetch soon.
        """

        self.aki = access_key_id
        self.sak = secret_access_key
        self.new_aki = None
        self.new_sak = None

    def fetch_new():
        """
        Method to create and fetch the new credentials from aws iam.
        """

    def disable_cur():
        """
        Disable our current keys using our new keys.
        """

def readin_current():
    """
    Args: 
        None
        (future, configure the aws creds file as an argument)
    Returns:
        A list of dictionaries, each dictionary has the, profile, Key ID, and Secret Key for each 
        account you have in your AWS Credentials file.abs
    Raises:
        None
    """
    #open up the file
    creds_read = open(creds_file)
    profiles=[]
    
    for line in creds_read:
        line = line.strip()
        if line.startswith("["):
            profile{}
            profile_name = line.strip("[]")
            profile.update({'profile': profile_name})
        elif line.startswith("aws_access_key_id"):
            key_id = line.lstrip("aws_access_key_id = ")
            profile.update({"key_id": key_id})
        elif line.startswith("aws_secret_access_key")
            secret_key = line.lstrip("aws_secret_access_key = ")
            profile.update({"secret_key": secret_key})
        else:
            pass    
        profiles.append(profile)

    creds_read.close()

    return(profiles)

def backup_old():
    """
    Take current credentials file and write it out to a credentials-{timestamp}
    file.
    """

def writeout_new():
    """
    Remove our old file and write out a new credentials file with our new
    """

def main():
    """
    Our main function, Here we grab our current creds and store them as objects
    for each we will create a new set of keys using our current keys.
    From there we'll write out our new credentials file and backup the old one.
    """

