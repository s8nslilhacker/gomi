#!/bin/bash

export PROFILE_NAME=$1

function help {
    echo "Usage: list-ec2.sh my-aws-profile" 

    echo "Requires: AWS CLI to be installed preferably from PIP"
}


function main {
    
    # Few tests to pass before running
    if [ ! -x `type aws | awk '{print $3}'` ]
    then
        echo "Please install aws cli, preferably using pip"
    elif [ -z $PROFILE_NAME ]
    then
        help
    else
    # Main aws cli call
    aws ec2 describe-instances --query "Reservations[*].Instances[*].{name: Tags[?Key=='Name'] | [0].Value, instance_id: InstanceId, ip_address: PrivateIpAddress}" --output table --profile $PROFILE_NAME
    fi
}

main
